package com.craftservices.manhunter;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.md_5.bungee.api.ChatColor;

public class ManHunterCommand implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("You must be a player to use this command!");
			return false;
		}
		Player p = (Player) sender;
		if(cmd.getName().equalsIgnoreCase("runner")) {
			if(args.length == 1) {
				Player t = Bukkit.getPlayer(args[0]);
				if(t != null) {
					Main.players.put(p.getUniqueId(), true);
				} else {
					p.sendMessage(ChatColor.RED + args[0] + " is not online!");
				}
			} else {
				p.sendMessage(ChatColor.RED + "Invalid usage, try /hunter <player>");
			}
		}
		if(cmd.getName().equalsIgnoreCase("hunter")) {
			if(args.length == 1) {
				Player t = Bukkit.getPlayer(args[0]);
				if(t != null) {
					Main.players.put(p.getUniqueId(), false);
					p.getInventory().addItem(new ItemStack(Material.COMPASS));
				} else {
					p.sendMessage(ChatColor.RED + args[0] + " is not online!");
				}
			} else {
				p.sendMessage(ChatColor.RED + "Invalid usage, try /hunter <player>");
			}
		}
		if(cmd.getName().equalsIgnoreCase("reset")) {
			if(args.length == 1) {
				Player t = Bukkit.getPlayer(args[0]);
				if(t != null) {
					Main.players.clear();
				} else {
					p.sendMessage(ChatColor.RED + args[0] + " is not online!");
				}
			} else {
				p.sendMessage(ChatColor.RED + "Invalid usage, try /hunter <player>");
			}
		}
		return false;
	}
}
