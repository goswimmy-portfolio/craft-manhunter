package com.craftservices.manhunter;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import net.md_5.bungee.api.ChatColor;

public class ManHunterEvent implements Listener {

	@EventHandler
	public void onKill(PlayerDeathEvent e) {
		Player p = e.getEntity().getPlayer();

		if (Main.players.containsKey(p.getUniqueId())) {
			if (Main.players.get(p.getUniqueId())) {
				Main.players.remove(p.getUniqueId());
				for (UUID uuid : Main.players.keySet()) {
					Player t = Bukkit.getPlayer(uuid);
					t.sendTitle(ChatColor.translateAlternateColorCodes('&', "&a&lVictory"),
							ChatColor.translateAlternateColorCodes('&', "&f" + p.getName() + " has been killed!"), 1,
							20, 1);
				}
				Main.players.clear();

			}
		}
	}
}
