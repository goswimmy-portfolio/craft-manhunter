package com.craftservices.manhunter;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Main extends JavaPlugin {

	public static HashMap<UUID, Boolean> players = new HashMap<>();

	public void onEnable() {
		getCommand("hunter").setExecutor(new ManHunterCommand());
		getCommand("runner").setExecutor(new ManHunterCommand());
		getCommand("restart").setExecutor(new ManHunterCommand());

		new BukkitRunnable() {
			public void run() {
				if (players.containsValue(true)) {
					Player runner = Bukkit.getPlayer(runner());
					if (runner != null) {
						for (UUID alluuid : players.keySet()) {
							Player all = Bukkit.getPlayer(alluuid);
							if (all != null)
								all.setCompassTarget(runner.getLocation());
						}
					} else {
						hunterwin(runner);
					}
				}
			}
		}.runTaskTimer(this, 1, 1);
	}

	public void hunterwin(Player runner) {
		for (UUID alluuid : players.keySet()) {
			if (!players.get(alluuid)) {
				Player p = Bukkit.getPlayer(alluuid);
				p.sendTitle("�a�lVictory", "�f" + runner.getName() + " died!", 1, 20, 1);
			}
		}
	}

	public UUID runner() {
		for (UUID alluuid : players.keySet()) {
			if (players.get(alluuid)) {
				return alluuid;
			}
		}
		return null;
	}
}
